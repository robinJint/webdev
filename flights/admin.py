from django.contrib import admin

from .import models


@admin.register(models.Carrier)
class CarrierAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Airport)
class AirportAdmin(admin.ModelAdmin):
    pass


class FlightEventsInline(admin.TabularInline):
    model = models.FlightEvents


class DelaysAdmin(admin.TabularInline):
    model = models.Delays


class MinuteDelaysAdmin(admin.TabularInline):
    model = models.MinuteDelays

@admin.register(models.Statistics)
class StatisticsAdmin(admin.ModelAdmin):
    # raw_id_fields = ['flights', 'minutes', 'delays']
    fields = ("date", "carrier", "airport")
    inlines = [
        FlightEventsInline,
        DelaysAdmin,
        MinuteDelaysAdmin,
    ]
    def get_queryset(self, request):
        print('qs')
        return super().get_queryset(request).select_related(
            "carrier", "airport"
        )
