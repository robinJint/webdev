from django.db import models


class Carrier(models.Model):
    code = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return "<{}> {}".format(self.code, self.name)


class Airport(models.Model):
    code = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return "<{}> {}".format(self.code, self.name)

class Statistics(models.Model):
    date = models.DateField(db_index=True)
    carrier = models.ForeignKey(Carrier, on_delete=models.CASCADE, db_index=True)
    airport = models.ForeignKey(Airport, on_delete=models.CASCADE, db_index=True)

    class Meta:
        unique_together = (("carrier", "airport", "date"),)

    def __str__(self):
        return "<{}> <{}> ({})".format(self.carrier.code, self.airport.code, self.date)

class FlightEvents(models.Model):
    cancelled = models.IntegerField()
    on_time = models.IntegerField()
    total = models.IntegerField()
    delayed = models.IntegerField()
    diverted = models.IntegerField()

    #  statistics = models.OneToOneField(Statistics, on_delete=models.CASCADE)
    statistics = models.OneToOneField(Statistics, on_delete=models.CASCADE, primary_key=True)

    @property
    def total(self):
        all_fields = [
            getattr(self, field)
            for field in ["cancelled", "on_time", "delayed", "diverted"]
        ]
        return sum(all_fields)


class Delays(models.Model):
    aircraft = models.IntegerField()
    weather = models.IntegerField()
    carrier = models.IntegerField()
    security = models.IntegerField()
    aviation = models.IntegerField()

    statistics = models.OneToOneField(Statistics, on_delete=models.CASCADE, primary_key=True)

    @property
    def total(self):
        all_fields = [
            getattr(self, field)
            for field in ["aircraft", "weather", "carrier", "security", "aviation"]
        ]
        sum(all_fields)


class MinuteDelays(models.Model):
    aircraft = models.IntegerField()
    weather = models.IntegerField()
    carrier = models.IntegerField()
    security = models.IntegerField()
    aviation = models.IntegerField()

    statistics = models.OneToOneField(Statistics, on_delete=models.CASCADE, primary_key=True)

    @property
    def total(self):
        all_fields = [
            getattr(self, field)
            for field in ["aircraft", "weather", "carrier", "security", "aviation"]
        ]
        sum(all_fields)



