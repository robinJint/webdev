import io

from django.core.management.base import BaseCommand
from django.db import transaction
from rest_framework.parsers import JSONParser

from flights.import_serializers import StatisticsSerializer


class Command(BaseCommand):
    help = 'import flights from a json file'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        with open(options["file"], "rb") as f:
            data = JSONParser().parse(f)
            serializer = StatisticsSerializer(data=data, many=True)
            serializer.is_valid(raise_exception=True)
            with transaction.atomic():
                serializer.save()
