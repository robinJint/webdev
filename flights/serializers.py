from datetime import date, datetime

from django.contrib.auth.models import Group, User
from rest_framework import serializers

from .models import ( Airport, Carrier, Delays, FlightEvents, MinuteDelays, Statistics)


class CarrierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Carrier
        fields = ("code", "name")


class AirportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Airport
        fields = ("code", "name")


class FlightEventSerializer(serializers.ModelSerializer):

    class Meta:
        model = FlightEvents
        fields = ("cancelled", "on_time", "delayed", "total", "diverted")
        extra_kwargs = {'total': {'read_only': True}}


class DelaysSerializer(serializers.ModelSerializer):

    class Meta:
        model = Delays
        fields = ("aircraft", "weather", "carrier", "security", "aviation")
        #  extra_kwargs = {'total': {'read_only': True}}


class MinuteDelaysSerializer(serializers.ModelSerializer):

    class Meta:
        model = MinuteDelays
        fields = ("aircraft", "weather", "carrier", "security", "aviation")


class StatisticsSerializer(serializers.ModelSerializer):
    airport = AirportSerializer(required=True)
    carrier = CarrierSerializer(required=True)
    flights = FlightEventSerializer(required=True, source="flightevents")
    delays = DelaysSerializer(required=True, )
    minutes = DelaysSerializer(required=True, source="minutedelays")

    class Meta:
        model = Statistics
        fields = ("date", "flights", "delays", "minutes", "carrier", "airport")

    def create(self, validated_data):
        airport_data = validated_data.pop("airport")
        carrier_data = validated_data.pop("carrier")
        flights_data = validated_data.pop("flights")
        minutes_data = validated_data.pop("minutes")
        delays_data = validated_data.pop("delays")
        date_field = validated_data.pop("date")

        airport, _ = Airport.objects.get_or_create(code=airport_data["code"], defaults=airport_data)
        carrier, _ = Carrier.objects.get_or_create(code=carrier_data["code"], defaults=carrier_data)

        res = Statistics.objects.create(airport=airport, carrier=carrier, date=date_field)

        FlightEvents.objects.create(statistics=res, **flights_data)
        MinuteDelays.objects.create(statistics=res, **minutes_data)
        Delays.objects.create(statistics=res, **delays_data)
        return res
