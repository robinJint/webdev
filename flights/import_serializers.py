from django.contrib.auth.models import User, Group
from django.utils.translation import gettext as _
from rest_framework import serializers

from .models import *

from datetime import datetime, date


class CarrierSerializer(serializers.ModelSerializer):

    class Meta:
        model = Carrier
        fields = ("code", "name")


class AirportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Airport
        fields = ("code", "name")


class FlightEventSerializer(serializers.ModelSerializer):

    class Meta:
        model = FlightEvents
        fields = ("cancelled", "delayed", "diverted")

    def __init__(self, **kwargs):
        self.fields["on time"] = serializers.IntegerField(required=True, source="on_time")
        super().__init__(**kwargs)


class DelaysSerializer(serializers.ModelSerializer):

    class Meta:
        model = Delays
        fields = ("weather", "carrier", "security")

    def __init__(self, **kwargs):
        self.fields["late aircraft"] = serializers.IntegerField(required=True, source="aircraft")
        self.fields["national aviation system"] = serializers.IntegerField(required=True, source="aviation")

        super().__init__(**kwargs)



class MinuteDelaysSerializer(serializers.ModelSerializer):

    class Meta:
        model = MinuteDelays
        fields = ("weather", "carrier", "security")

    def __init__(self, **kwargs):
        self.fields["late aircraft"] = serializers.IntegerField(required=True, source="aircraft")
        self.fields["national aviation system"] = serializers.IntegerField(required=True, source="aviation")

        super().__init__(**kwargs)


class TimeField(serializers.Field):
    default_error_messages = {
        "not_a_dict": _("Expected a dict of 'year' and 'month'."),
        "no_year": _("Expected a dict of 'year' and 'month'."),
        "no_month": _("Expected a dict of 'year' and 'month'."),
    }

    def to_internal_value(self, data):
        if not isinstance(data, dict):
            self.fail("not_a_dict")

        year = data.get("year")
        month = data.get("month")

        if year is None:
            self.fail("no_year")
        if month is None:
            self.fail("no_month")

        return date(year, month, 1)

    def to_representation(self, value):
        return {
            "year": value.year,
            "month": value.month,
        }


class StatisticsSerializer(serializers.ModelSerializer):
    airport = AirportSerializer(required=True)
    flights = FlightEventSerializer(required=True)
    carrier = CarrierSerializer(required=True)
    time = TimeField(required=True, source="date")

    def __init__(self, **kwargs):
        self.fields["# of delays"] = DelaysSerializer(required=True, source="delays")
        self.fields["minutes delayed"] = MinuteDelaysSerializer(required=True, source="minutes")

        super().__init__(**kwargs)

    class Meta:
        model = Statistics
        fields = ("time", "flights", "carrier", "airport")

    def to_internal_value(self, data):
        stats = data.pop("statistics")

        data.update(stats)
        return super().to_internal_value(data)

    def create(self, validated_data):
        airport_data = validated_data.pop("airport")
        carrier_data = validated_data.pop("carrier")

        flights_data = validated_data.pop("flights")
        minutes_data = validated_data.pop("minutes")
        delays_data = validated_data.pop("delays")

        date = validated_data.pop("date")

        airport, _ = Airport.objects.get_or_create(code=airport_data["code"], defaults=airport_data)
        carrier, _ = Carrier.objects.get_or_create(code=carrier_data["code"], defaults=carrier_data)

        prev = Statistics.objects.filter(airport=airport, carrier=carrier, date=date).first()
        if prev:
            prev.delete()
        res = Statistics.objects.create(airport=airport, carrier=carrier, date=date)

        flights = FlightEvents.objects.create(statistics=res, **flights_data)
        minutes = MinuteDelays.objects.create(statistics=res, **minutes_data)
        delays = Delays.objects.create(statistics=res, **delays_data)


        return res

