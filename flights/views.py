import coreapi
from django.db.models import Sum
from django.db.models.functions import Coalesce
from rest_framework import generics, mixins, serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.schemas import AutoSchema

from .import_serializers import TimeField
from .models import Airport, Carrier, Statistics
from .serializers import AirportSerializer, CarrierSerializer, StatisticsSerializer


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "carriers": reverse("carriers", request=request, format=format),
            "airports": reverse("airports", request=request, format=format),
        }
    )


class CarrierList(generics.ListAPIView):
    queryset = Carrier.objects.all()
    serializer_class = CarrierSerializer


class AirportList(generics.ListAPIView):
    queryset = Airport.objects.all()
    serializer_class = AirportSerializer


class StatisticsView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Statistics.objects.all()
    serializer_class = StatisticsSerializer

    schema = AutoSchema(
        manual_fields=[
            coreapi.Field("times", required=False, location="form", type="dict"),
            coreapi.Field("airports", required=False, location="form", type="int"),
        ]
    )

    class Args(serializers.Serializer):
        times = serializers.ListField(child=TimeField(), required=False)
        airports = serializers.ListField(child=serializers.CharField(), required=False)

    def nested(self, data, name, value):
        path = name.split("__")

        current = data
        for part in path[:-1]:
            if not current.get(part):
                current[part] = {}
            current = current[part]
        current[path[-1]] = value

    def nested_aggregate(self, blocks, qs, data):
        aggregate_args = {}

        for block, fields in blocks.items():
            for field in fields:
                name = "{}__{}".format(block, field)
                aggregate_args[name] = Coalesce(Sum(name), 0)

        stats = qs.aggregate(**aggregate_args)

        data = {}
        for name, value in stats.items():
            self.nested(data, name, value)

        return data


    def get(self, request, carrier):
        serializer = self.Args(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        search_args = {}
        if data.get("times"):
            search_args["date__in"] = data["times"]
        if data.get("airports"):
            search_args["airport__code__in"] = data["airports"]

        search_args["carrier__code"] = carrier

        blocks = {
            "flightevents": ["cancelled", "on_time", "delayed", "diverted"],
            "delays": ["aircraft", "wheather", "carrier", "security", "aviation"],
            "minutedelays": ["aircraft", "wheather", "carrier", "security", "aviation"],
        }

        qs = Statistics.objects.filter(**search_args)
        stats = self.nested_aggregate(blocks, qs, data)

        return Response(stats)
